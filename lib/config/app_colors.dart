import 'package:flutter/material.dart';

class AppColor {
  static Color backgroundColor = Color(0xFF000000);
  static Color appbarColor = Color(0xFF1A1417);
  static Color submitButtonBgColor = Color(0xFFAC0012);
  static Color submitButtonBgDisabledColor = Color(0xFF34000A);
  static Color submitButtonColor = Color(0xFFFFFFFF);
  static Color headerTextColor = Color(0xFFFFFFFF);
  static Color normalTextColor = Color(0xFF705463);
}

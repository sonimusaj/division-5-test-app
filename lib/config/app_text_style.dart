import 'package:flutter/material.dart';

import 'app_colors.dart';

class AppTextStyle {
  static TextStyle appBarTextStyle = TextStyle(
      fontSize: 18,
      color: Colors.white,
      fontWeight: FontWeight.w900,
    );

  static TextStyle appBarActionTextStyle = TextStyle(
      fontSize: 14,
      color: AppColor.normalTextColor,
      fontWeight: FontWeight.w400,
    );

   static TextStyle titleStyle = TextStyle(
        color: AppColor.headerTextColor,
        fontSize: 30,
        fontWeight: FontWeight.w900
    );

   static TextStyle headerStyle = TextStyle(
        color: Colors.white,
        fontSize: 18,
        letterSpacing: 1.1,
        fontWeight: FontWeight.w900
    );

   static TextStyle noteStyle = TextStyle(
        color: AppColor.normalTextColor,
        fontSize: 12,
        letterSpacing: 1.1,
        fontWeight: FontWeight.normal
    );

   static TextStyle textFieldTitleStyle = TextStyle(
        color: AppColor.normalTextColor,
        fontSize: 16,
        letterSpacing: 1.1,
        fontWeight: FontWeight.w400
    );

   static TextStyle textFieldBigTitleStyle = TextStyle(
        color: Colors.white,
        fontSize: 20,
        letterSpacing: 1.1,
        fontWeight: FontWeight.w900
    );

   static TextStyle submitButtonStyle = TextStyle(
        color: AppColor.submitButtonColor,
        fontSize: 16,
        fontWeight: FontWeight.bold,
    );
}

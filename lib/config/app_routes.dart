import 'package:flutter/material.dart';
import 'package:otto_wilde/views/pages/account_page.dart';
import 'package:otto_wilde/views/pages/onboarding_page.dart';
import 'package:otto_wilde/views/pages/signin_page.dart';
import 'package:otto_wilde/views/pages/signup_page.dart';
import 'package:otto_wilde/views/pages/splash_screen.dart';

final routes = {
  AppRoute.SPLASH: (BuildContext context) => SplashScreen(),
  AppRoute.SIGN_IN: (BuildContext context) => SignInPage(),
  AppRoute.SIGN_UP: (BuildContext context) => SignUpPage(),
  AppRoute.ONBOARDING: (BuildContext context) => OnboardingPage(),
  AppRoute.ACCOUNT: (BuildContext context) => AccountPage(),
};

class AppRoute {
  static const SPLASH = '/';
  static const SIGN_IN = '/sign_in';
  static const SIGN_UP = '/sign_up';
  static const ONBOARDING = '/onboarding';
  static const ACCOUNT = '/account';
}

import 'package:flutter/material.dart';
import 'package:otto_wilde/bloc/page_controller.dart';
import 'package:otto_wilde/config/app_routes.dart';
import 'package:otto_wilde/model/networking/user.dart';
import 'package:otto_wilde/networking/providers/user_provider.dart';
import 'package:otto_wilde/utils/dialog_utils.dart';
import 'package:otto_wilde/utils/localization/app_localizations.dart';
import 'package:otto_wilde/utils/shared_preferences.dart';
import 'package:rxdart/rxdart.dart';

class AccountPageController implements BlocPageController{
  UserProvider userProvider = UserProvider();

  TextEditingController emailController = TextEditingController();
  TextEditingController firstNameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController streetController = TextEditingController();
  TextEditingController postalCodeController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController countryController = TextEditingController();

  BehaviorSubject<bool> _loadingSubject = BehaviorSubject();
  Stream<bool> get loadingStream => _loadingSubject.stream;

  AccountPageController();

  logout(BuildContext context){
    SharedPreferencesUtil.saveLoggedInUserId("");
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder:
        routes[AppRoute.SPLASH]), (Route<dynamic> route) => false);
  }

  getAccountData(BuildContext context) async {
      _loadingSubject.add(true);
      var accountId = await SharedPreferencesUtil.getLoggedInUserId();
      userProvider.getAccountData(accountId)
          .then((value){
        _loadingSubject.add(false);
        _initUserData(value);
      }).catchError((error){
        _loadingSubject.add(false);
        DialogUtils.showMessageDialog(context, AppLocalizations.of(context).trans("attention"), error.toString());
      });
  }
    

  @override
  dispose() {
  }

  void _initUserData(User user) {
    emailController.text = user.emailAddress;
    firstNameController.text = user.firstName;
    phoneController.text = user.phoneNumber;
    streetController.text = user.street;
    cityController.text = user.city;
    postalCodeController.text = user.postalCode;
    countryController.text = user.country;
  }
}
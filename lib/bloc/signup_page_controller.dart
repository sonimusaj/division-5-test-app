import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:otto_wilde/bloc/page_controller.dart';
import 'package:otto_wilde/config/app_routes.dart';
import 'package:otto_wilde/model/networking/user.dart';
import 'package:otto_wilde/networking/providers/user_provider.dart';
import 'package:otto_wilde/utils/dialog_utils.dart';
import 'package:otto_wilde/utils/localization/app_localizations.dart';
import 'package:otto_wilde/utils/utils.dart';
import 'package:rxdart/rxdart.dart';

class SignUpPageController implements BlocPageController{
  UserProvider userProvider = UserProvider();

  TextEditingController emailController = TextEditingController();
  TextEditingController emailConfirmationController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController passwordConfirmationController = TextEditingController();
  TextEditingController firstNameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController streetController = TextEditingController();
  TextEditingController postalCodeController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController countryController = TextEditingController();

  BehaviorSubject<bool> _requiredDataEntered = BehaviorSubject();
  Stream<bool> get requiredDataEnteredStream => _requiredDataEntered.stream;
  BehaviorSubject<bool> _loadingSubject = BehaviorSubject();
  Stream<bool> get loadingStream => _loadingSubject.stream;

  SignUpPageController();

  Future<bool> createUser(){
    var user = User(
        emailAddress: emailController.text.toLowerCase(),
        password: passwordController.text,
        firstName: firstNameController.text,
        phoneNumber: phoneController.text ?? "",
        street: streetController.text ?? "",
        city: cityController.text ?? "",
        postalCode: postalCodeController.text ?? "",
        country: countryController.text ?? ""
    );
    return userProvider.createUser(user);
  }

  String validateData(BuildContext context) {
    var emailAddress = emailController.text.toLowerCase();
    var emailConfirmation = emailConfirmationController.text;
    var password = passwordController.text;
    var passwordConfirmation = passwordConfirmationController.text;

    if(emailAddress != emailConfirmation) return AppLocalizations.of(context).trans("email_dont_match");
    if(!EmailValidator.validate(emailAddress)) return AppLocalizations.of(context).trans("email_not_correct");
    if(password != passwordConfirmation) return AppLocalizations.of(context).trans("password_dont_match");
    return null;
  }

  requiredDataOnChangedListener(_){
    var emailAddress = emailController.text.toLowerCase();
    var emailConfirmation = emailConfirmationController.text.toLowerCase();
    var password = passwordController.text;
    var passwordConfirmation = passwordConfirmationController.text;
    var firstName = firstNameController.text;

    var previousState = _requiredDataEntered.value ?? false;
    if(emailAddress.isNotEmpty && password.isNotEmpty && emailConfirmation.isNotEmpty && passwordConfirmation.isNotEmpty && firstName.isNotEmpty){
      if(!previousState)_requiredDataEntered.add(true);
    }else{
      if(previousState)_requiredDataEntered.add(false);
    }

  }


  void signUp(BuildContext context) {
    FocusScope.of(context).unfocus();
    _loadingSubject.add(true);
    var validation = validateData(context);
    if(validation != null){
      _loadingSubject.add(false);
      Utils.showToast(context, validation);
      return;
    }
    createUser()
        .then((value){
      _loadingSubject.add(false);
      //DialogUtils.showMessageDialog(context, AppLocalizations.of(context).trans("success"), AppLocalizations.of(context).trans("user_created"));
      //user registered
      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder:
      routes[AppRoute.ACCOUNT]), (Route<dynamic> route) => false);
    }).catchError((error){
      _loadingSubject.add(false);
      DialogUtils.showMessageDialog(context, AppLocalizations.of(context).trans("attention"), error.toString());
    });
  }

  @override
  dispose() {
  }
}
import 'package:flutter/material.dart';
import 'package:otto_wilde/bloc/page_controller.dart';
import 'package:otto_wilde/config/app_routes.dart';
import 'package:otto_wilde/networking/providers/user_provider.dart';
import 'package:otto_wilde/utils/dialog_utils.dart';
import 'package:otto_wilde/utils/localization/app_localizations.dart';
import 'package:otto_wilde/utils/shared_preferences.dart';
import 'package:rxdart/rxdart.dart';

class SignInPageController implements BlocPageController{
  UserProvider userProvider = UserProvider();

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  BehaviorSubject<bool> _requiredDataEntered = BehaviorSubject();
  Stream<bool> get requiredDataEnteredStream => _requiredDataEntered.stream;
  BehaviorSubject<bool> _loadingSubject = BehaviorSubject();
  Stream<bool> get loadingStream => _loadingSubject.stream;

  SignInPageController();

  requiredDataOnChangedListener(_){
    var emailAddress = emailController.text.toLowerCase();
    var password = passwordController.text;

    var previousState = _requiredDataEntered.value ?? false;
    if(emailAddress.isNotEmpty && password.isNotEmpty){
      if(!previousState)_requiredDataEntered.add(true);
    }else{
      if(previousState)_requiredDataEntered.add(false);
    }

  }

  void signIn(BuildContext context) {
    FocusScope.of(context).unfocus();
    _loadingSubject.add(true);
    userProvider.signin(emailController.text.toLowerCase(), passwordController.text)
        .then((value){
          _loadingSubject.add(false);

          //user logged in
          Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder:
          routes[AppRoute.ACCOUNT]), (Route<dynamic> route) => false);
        }).catchError((error){
          _loadingSubject.add(false);
          DialogUtils.showMessageDialog(context, AppLocalizations.of(context).trans("attention"), error.toString());
        });
  }

  @override
  dispose() {
  }
}
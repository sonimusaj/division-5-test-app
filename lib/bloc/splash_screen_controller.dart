import 'package:flutter/material.dart';
import 'package:otto_wilde/bloc/page_controller.dart';
import 'package:otto_wilde/utils/shared_preferences.dart';
import 'package:otto_wilde/views/pages/account_page.dart';
import 'package:otto_wilde/views/pages/onboarding_page.dart';

class SplashScreenController implements BlocPageController{

  SplashScreenController();

  navigateToNextPage(BuildContext context) async {
    var duration = 2000;

    var loggedInUser =  await SharedPreferencesUtil.getLoggedInUserId() != "";

    Future.delayed(Duration(milliseconds: duration), (){
      Navigator.of(context).pushReplacement(PageRouteBuilder(transitionDuration: Duration(milliseconds: duration), pageBuilder: (_, __, ___) => loggedInUser ? AccountPage() : OnboardingPage()));
    });
  }

  @override
  dispose() {
  }
}
import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:firebase_database/firebase_database.dart';
class NetworkUtil {
  static NetworkUtil _instance = new NetworkUtil.internal();

  NetworkUtil.internal();

  factory NetworkUtil() => _instance;

  Future<bool> isNetworkAvailable() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    return connectivityResult != ConnectivityResult.none;
  }

   DatabaseReference usersDbReference = FirebaseDatabase.instance.reference().child('users');

}

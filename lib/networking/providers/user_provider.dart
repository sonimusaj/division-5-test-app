import 'package:firebase_database/firebase_database.dart';
import 'package:otto_wilde/model/networking/user.dart';
import 'package:otto_wilde/utils/shared_preferences.dart';
import 'dart:convert';

import '../network_util.dart';

class UserProvider {
  NetworkUtil _networkUtil;

  UserProvider() {
    _networkUtil = NetworkUtil();
  }

  _getUserDbId(String email){
    return email.replaceAll("@", "").replaceAll(".", "").replaceAll("_", "").replaceAll("-", "");
  }

  Future<bool> createUser(User user) async {
    if(!(await _networkUtil.isNetworkAvailable())) {
      throw Exception("No internet connection!");
    }
    var userExists = await _networkUtil.usersDbReference.child(_getUserDbId(user.emailAddress)).once().then((value) => value.value != null);
    if(userExists){
      throw Exception("User already exists!");
    }
    return _networkUtil.usersDbReference.child(_getUserDbId(user.emailAddress))
        .set(user.toJson()).then((value) async {
      await SharedPreferencesUtil.saveLoggedInUserId(_getUserDbId(user.emailAddress));
      return true;
        });
  }

  Future<User> signin(String email, String password) async {
    if(!(await _networkUtil.isNetworkAvailable())) {
      throw Exception("No internet connection!");
    }

    try {
      User user = await _networkUtil.usersDbReference.child(_getUserDbId(email))
          .once()
          .then((value) => User.fromJson(Map<String, dynamic>.from(value.value)));
      if(user.password != password){
        throw Exception("Email or password not correct!");
      }
      await SharedPreferencesUtil.saveLoggedInUserId(_getUserDbId(user.emailAddress));
      return user;
    }catch(e){
      throw Exception("Email or password not correct!");
    }

  }

  Future<User> getAccountData(String userId) async {
    if(!(await _networkUtil.isNetworkAvailable())) {
      throw Exception("No internet connection!");
    }

    User user = await _networkUtil.usersDbReference.child(userId)
        .once()
        .then((value) => User.fromJson(Map<String, dynamic>.from(value.value)));
    return user;

  }


}


class User {
  String emailAddress;
  String password;
  String phoneNumber;
  String street;
  String city;
  String postalCode;
  String country;
  String firstName;


  User({this.emailAddress, this.password, this.phoneNumber, this.street,
      this.city, this.postalCode, this.country, this.firstName});

  User.fromJson(Map<String, dynamic> json) {
    if(json == null) return;
    emailAddress = (json['email'] ?? "").toString();
    password = (json['password'] ?? "").toString();
    phoneNumber = (json['phone_number'] ?? "").toString();
    street = (json['street'] ?? "").toString();
    city = (json['city'] ?? "").toString();
    postalCode = (json['postal_code'] ?? "").toString();
    country = (json['country'] ?? "").toString();
    firstName = (json['first_name'] ?? "").toString();

  }

  Map<String, dynamic> toJson(){
    return {
      'email': emailAddress,
      'password': password,
      'phone_number': phoneNumber,
      'street': street,
      'city': city,
      'postal_code': postalCode,
      'country': country,
      'first_name': firstName,
    };
  }
}

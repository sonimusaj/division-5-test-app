import 'package:flutter/material.dart';

class LoadingContainer extends StatelessWidget {

  Widget child;
  Stream loadingStream;

  LoadingContainer({this.child, this.loadingStream});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        child,
        StreamBuilder(
          stream: loadingStream,
          builder: (context, snapshot){
            return (snapshot.data ?? false)
                ? Container(
                color: Colors.black.withOpacity(0.5),
                child: Center(child: CircularProgressIndicator()))
                : Container();
          },
        )
      ],
    );
  }
}

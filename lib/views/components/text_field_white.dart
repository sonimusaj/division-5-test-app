import 'package:flutter/material.dart';
import 'package:otto_wilde/config/app_text_style.dart';
import 'package:otto_wilde/utils/localization/app_localizations.dart';

class TextFieldWhite extends StatelessWidget {

  String title;
  bool required;
  bool bigTitle;
  bool obscureText;
  bool enabled;
  Function onChanged;
  Function onSubmitted;
  FocusNode focusNode;
  FocusNode nextFocusNode;
  TextEditingController controller;

  TextFieldWhite(
      this.title,
      {
        this.required = false,
        this.bigTitle = false,
        this.obscureText = false,
        this.enabled = true,
        this.onChanged,
        this.focusNode,
        this.nextFocusNode,
        this.onSubmitted,
        this.controller
      });

  @override
  Widget build(BuildContext context) {

    var textFieldDecoration = OutlineInputBorder(
      borderSide: BorderSide(color: Colors.white),
      borderRadius: BorderRadius.circular(3),
    );

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "$title${required ? "*": ""}",
          style: bigTitle ? AppTextStyle.textFieldBigTitleStyle : AppTextStyle.textFieldTitleStyle,
        ),
        SizedBox(height: 8,),
        TextFormField(
          enabled: enabled,
          controller: controller,
          focusNode: focusNode,
          onFieldSubmitted: (_) {
            nextFocusNode != null
                ? nextFocusNode.requestFocus()
                : FocusScope.of(context).unfocus();
            onSubmitted();
          },
          obscureText: obscureText,
          onChanged: onChanged != null ? onChanged : null,
          textInputAction: nextFocusNode != null
              ? TextInputAction.next
              : TextInputAction.done,

          decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(horizontal: 10),
            focusedBorder: textFieldDecoration,
            enabledBorder: textFieldDecoration,
            filled: true,
            fillColor: Colors.white,
          ),
        ),
      ],
    );
  }
}

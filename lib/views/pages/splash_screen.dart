
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:otto_wilde/bloc/splash_screen_controller.dart';
import 'package:otto_wilde/config/app_colors.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  SplashScreenController _controller;

  @override
  void initState() {
    super.initState();

    _controller = SplashScreenController();
    _controller.navigateToNextPage(context);
  }

  @override
  Widget build(BuildContext context) {

    return SafeArea(
      child: Scaffold(
        backgroundColor: AppColor.backgroundColor,
        body: Align(
          alignment: Alignment.center,
          child: Hero(
            tag: "app_logo",
            child: SvgPicture.asset("assets/images/otto-logo.svg", fit: BoxFit.fitWidth, width: 120),
          ),
        ),
      ),
    );
  }
}

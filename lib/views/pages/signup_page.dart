
import 'dart:io';

import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:otto_wilde/bloc/signup_page_controller.dart';
import 'package:otto_wilde/config/app_colors.dart';
import 'package:otto_wilde/config/app_text_style.dart';
import 'package:otto_wilde/model/networking/user.dart';
import 'package:otto_wilde/utils/dialog_utils.dart';
import 'package:otto_wilde/utils/localization/app_localizations.dart';
import 'package:rxdart/rxdart.dart';

import '../components/loading_container.dart';
import '../components/text_field_white.dart';

class SignUpPage extends StatefulWidget {

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> with TickerProviderStateMixin{

  SignUpPageController _controller;
  double dataContainerHeight = 0;
  double contentSpacing = 25;
  FocusNode emailFocusNode = FocusNode();
  FocusNode emailConfirmationFocusNode = FocusNode();
  FocusNode passwordFocusNode = FocusNode();
  FocusNode passwordConfirmationFocusNode = FocusNode();
  FocusNode firstNameFocusNode = FocusNode();
  FocusNode phoneFocusNode = FocusNode();
  FocusNode streetFocusNode = FocusNode();
  FocusNode postalCodeFocusNode = FocusNode();
  FocusNode cityFocusNode = FocusNode();
  FocusNode countryFocusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    _controller = SignUpPageController();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: AppColor.backgroundColor,
      appBar: AppBar(
        brightness: Brightness.dark, // or use Brightness.dark
        leading: IconButton(
          onPressed: (){
            Navigator.of(context).pop();
          },
          icon: Icon(Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back, color: Colors.white,),),
        centerTitle: true,
        title: Text(AppLocalizations.of(context).trans("sign_up").toUpperCase(), textAlign: TextAlign.center, style: AppTextStyle.appBarTextStyle,),
        backgroundColor: AppColor.appbarColor,
        actions: [
          Platform.isIOS ? TextButton(
              onPressed: (){
                _controller.signUp(context);
              },
              child: Text(AppLocalizations.of(context).trans("done"), style: AppTextStyle.appBarActionTextStyle,)
          ) : SizedBox()
        ],
      ),
      body: LoadingContainer(
        loadingStream: _controller.loadingStream,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(
                    AppLocalizations.of(context).trans("your_info").toUpperCase(),
                  style: AppTextStyle.headerStyle,
                ),
                SizedBox(height: 15,),
                Text(
                  AppLocalizations.of(context).trans("required_field"),
                  style: AppTextStyle.noteStyle,
                ),
                SizedBox(height: contentSpacing,),
                TextFieldWhite(AppLocalizations.of(context).trans("email_address"), onChanged: _controller.requiredDataOnChangedListener, controller: _controller.emailController, required: true, focusNode: emailFocusNode, nextFocusNode: emailConfirmationFocusNode,),
                SizedBox(height: contentSpacing,),
                TextFieldWhite(AppLocalizations.of(context).trans("email_address"), onChanged: _controller.requiredDataOnChangedListener, controller: _controller.emailConfirmationController, required: true, focusNode: emailConfirmationFocusNode, nextFocusNode: passwordFocusNode,),
                SizedBox(height: contentSpacing,),
                TextFieldWhite(AppLocalizations.of(context).trans("password"), onChanged: _controller.requiredDataOnChangedListener, controller: _controller.passwordController, required: true, obscureText: true, focusNode: passwordFocusNode, nextFocusNode: passwordConfirmationFocusNode,),
                SizedBox(height: contentSpacing,),
                TextFieldWhite(AppLocalizations.of(context).trans("password"), onChanged: _controller.requiredDataOnChangedListener, controller: _controller.passwordConfirmationController, required: true, obscureText: true, focusNode: passwordConfirmationFocusNode, nextFocusNode: firstNameFocusNode,),
                SizedBox(height: contentSpacing,),
                TextFieldWhite(AppLocalizations.of(context).trans("first_name"), onChanged: _controller.requiredDataOnChangedListener, controller: _controller.firstNameController, required: true, focusNode: firstNameFocusNode, nextFocusNode: phoneFocusNode,),
                SizedBox(height: contentSpacing,),
                TextFieldWhite(AppLocalizations.of(context).trans("phone_number"), controller: _controller.phoneController, focusNode: phoneFocusNode, nextFocusNode: streetFocusNode,),
                SizedBox(height: contentSpacing,),
                TextFieldWhite(AppLocalizations.of(context).trans("street"), controller: _controller.streetController, focusNode: streetFocusNode, nextFocusNode: postalCodeFocusNode,),
                SizedBox(height: contentSpacing,),
                TextFieldWhite(AppLocalizations.of(context).trans("postal_code"), controller: _controller.postalCodeController, focusNode: postalCodeFocusNode, nextFocusNode: cityFocusNode,),
                SizedBox(height: contentSpacing,),
                TextFieldWhite(AppLocalizations.of(context).trans("city"), controller: _controller.cityController, focusNode: cityFocusNode, nextFocusNode: countryFocusNode,),
                SizedBox(height: contentSpacing,),
                TextFieldWhite(AppLocalizations.of(context).trans("country"), controller: _controller.countryController, focusNode: countryFocusNode),
                SizedBox(height: contentSpacing,),
                Platform.isAndroid ? Container(
                  height: 50,
                  child: StreamBuilder(
                    stream: _controller.requiredDataEnteredStream,
                    builder: (context, snapshot){
                      return RaisedButton(
                        onPressed: (snapshot.data ?? false) ? (){
                          _controller.signUp(context);
                        } : null,
                        disabledColor: AppColor.submitButtonBgDisabledColor,
                        color: AppColor.submitButtonBgColor,
                        child: Text(AppLocalizations.of(context).trans("sign_up").toUpperCase(), style: AppTextStyle.submitButtonStyle,),
                      );
                    },
                  ),
                ) : SizedBox(),
              ],
            ),
          ),
        ),
      ),
    );
  }

}

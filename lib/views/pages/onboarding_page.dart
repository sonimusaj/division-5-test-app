
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:otto_wilde/bloc/onboarding_page_controller.dart';
import 'package:otto_wilde/config/app_colors.dart';
import 'package:otto_wilde/config/app_routes.dart';
import 'package:otto_wilde/config/app_text_style.dart';
import 'package:otto_wilde/utils/localization/app_localizations.dart';
import 'package:otto_wilde/utils/utils.dart';


class OnboardingPage extends StatefulWidget {

  @override
  _OnboardingPageState createState() => _OnboardingPageState();
}

class _OnboardingPageState extends State<OnboardingPage> with TickerProviderStateMixin{

  OnboardingPageController controller;
  double dataContainerHeight = 0;

  @override
  void initState() {
    super.initState();
    controller = OnboardingPageController();

    Future.delayed(Duration(milliseconds: 400), (){
      setState(() {
        dataContainerHeight = 340;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuerySize = MediaQuery.of(context);
    return SafeArea(
        child: Scaffold(
          backgroundColor: AppColor.backgroundColor,
      body: Container(
        height: mediaQuerySize.size.height - mediaQuerySize.padding.vertical,
        child: Stack(
          children: [
            Positioned(
              top: 60,
              left: 0,
              right: 0,
              child: Align(
                alignment: Alignment.center,
                child: Hero(
                  tag: "app_logo",
                  child: SvgPicture.asset("assets/images/otto-logo.svg", fit: BoxFit.fitWidth, width: 70,),),
              ),),
            Positioned(
              left: 0,
                right: 0,
                bottom: 10,
                child:
            Align(
              alignment: Alignment.center,
              child: AnimatedContainer(
                  duration: Duration(milliseconds: 1500),
                  height: dataContainerHeight,
                  curve: Curves.fastOutSlowIn,
                  padding: EdgeInsets.only(top: 20, bottom: 30, left: 20, right: 20),
                  margin: EdgeInsets.only(left: 30, right: 30),
                  child: SingleChildScrollView( //fix for overlapping in debug mode
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Container(
                          child: Text(AppLocalizations.of(context).trans("onboarding_title").toUpperCase(), textAlign: TextAlign.center, style: AppTextStyle.titleStyle)
                        ),
                        SizedBox(height: 20),
                        Container(
                          height: 50,
                          child: RaisedButton(
                            onPressed: (){
                              Navigator.of(context).push(
                                MaterialPageRoute(builder: routes[AppRoute.SIGN_UP])
                              );
                            },
                            color: AppColor.submitButtonBgColor,
                            child: Text(AppLocalizations.of(context).trans("sign_up").toUpperCase(), style: AppTextStyle.submitButtonStyle,),
                          ),
                        ),
                        SizedBox(height: 15,),
                        InkWell(
                          onTap: (){
                            Utils.showToast(context, "Not implemented");
                          },
                          child: Container(
                            height: 50,
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.white, width: 2),
                              color: Colors.transparent,
                              shape: BoxShape.rectangle,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(FontAwesomeIcons.facebookF, color: Colors.white, size: 15,),
                                SizedBox(width: 10,),
                                Text(AppLocalizations.of(context).trans("sign_up_fb").toUpperCase(), style: AppTextStyle.submitButtonStyle,)
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: 10,),
                        Container(
                          height: 50,
                          child: TextButton(
                            onPressed: (){
                              Navigator.of(context).push(
                                  MaterialPageRoute(builder: routes[AppRoute.SIGN_IN])
                              );
                            },
                            child: Text(AppLocalizations.of(context).trans("sign_in").toUpperCase(), style: AppTextStyle.submitButtonStyle,),
                          ),
                        ),
                      ],
                    ),
                  )
              ),
            ))
          ],
        ),
      ),
    ));
  }

}


import 'dart:io';

import 'package:flutter/material.dart';
import 'package:otto_wilde/bloc/signin_page_controller.dart';
import 'package:otto_wilde/config/app_colors.dart';
import 'package:otto_wilde/config/app_text_style.dart';
import 'package:otto_wilde/utils/localization/app_localizations.dart';
import 'package:otto_wilde/utils/utils.dart';
import 'package:otto_wilde/views/components/loading_container.dart';
import 'package:otto_wilde/views/components/text_field_white.dart';

class SignInPage extends StatefulWidget {

  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> with TickerProviderStateMixin{

  SignInPageController _controller;
  double contentSpacing = 25;
  FocusNode emailFocusNode = FocusNode();
  FocusNode passwordFocusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    _controller = SignInPageController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundColor,
      appBar: AppBar(
        brightness: Brightness.dark, // or use Brightness.dark
        leading: IconButton(
          onPressed: (){
            Navigator.of(context).pop();
          },
          icon: Icon(Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back, color: Colors.white,),),
        centerTitle: true,
        title: Text(AppLocalizations.of(context).trans("login").toUpperCase(), textAlign: TextAlign.center, style: AppTextStyle.appBarTextStyle,),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: LoadingContainer(
        loadingStream: _controller.loadingStream,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                TextFieldWhite(AppLocalizations.of(context).trans("email_address"), bigTitle: true, controller: _controller.emailController, onChanged: _controller.requiredDataOnChangedListener, focusNode: emailFocusNode, nextFocusNode: passwordFocusNode,),
                SizedBox(height: contentSpacing,),
                TextFieldWhite(AppLocalizations.of(context).trans("password"), bigTitle: true, obscureText: true, onChanged: _controller.requiredDataOnChangedListener, controller: _controller.passwordController, focusNode: passwordFocusNode,),
                SizedBox(height: contentSpacing,),
                Platform.isAndroid ? Container(
                  height: 50,
                  child: StreamBuilder(
                    stream: _controller.requiredDataEnteredStream,
                    builder: (context, snapshot){
                      return RaisedButton(
                        onPressed: (snapshot.data ?? false ) ? (){
                          _controller.signIn(context);
                        } : null,
                        disabledColor: AppColor.submitButtonBgDisabledColor,
                        color: AppColor.submitButtonBgColor,
                        child: Text(AppLocalizations.of(context).trans("sign_in").toUpperCase(), style: AppTextStyle.submitButtonStyle,),
                      );
                    },
                  ),
                ) : SizedBox(),
                SizedBox(height: 10,),
                Container(
                  height: 50,
                  child: TextButton(
                    onPressed: (){
                      Utils.showToast(context, "Not implemented");
                    },
                    child: Text(AppLocalizations.of(context).trans("forgot_password"), style: AppTextStyle.submitButtonStyle,),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

}

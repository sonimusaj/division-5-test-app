
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:otto_wilde/bloc/account_page_controller.dart';
import 'package:otto_wilde/config/app_colors.dart';
import 'package:otto_wilde/config/app_text_style.dart';
import 'package:otto_wilde/utils/dialog_utils.dart';
import 'package:otto_wilde/utils/localization/app_localizations.dart';
import 'package:otto_wilde/views/components/loading_container.dart';
import 'package:otto_wilde/views/components/text_field_white.dart';

class AccountPage extends StatefulWidget {

  @override
  _AccountPageState createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> with TickerProviderStateMixin{

  AccountPageController _controller;
  double dataContainerHeight = 0;
  double contentSpacing = 25;

  @override
  void initState() {
    super.initState();
    _controller = AccountPageController();
    _controller.getAccountData(context);
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: AppColor.backgroundColor,
      appBar: AppBar(
        brightness: Brightness.dark, // or use Brightness.dark
        centerTitle: true,
        leading: Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Hero(
            tag: "app_logo",
            child: SvgPicture.asset("assets/images/otto-logo.svg", fit: BoxFit.contain,),
      ),
        ),
        title: Text(AppLocalizations.of(context).trans("profile_page").toUpperCase(), textAlign: TextAlign.center, style: AppTextStyle.appBarTextStyle,),
        backgroundColor: AppColor.backgroundColor,
        actions: [
          IconButton(icon: Icon(Icons.logout), onPressed: (){
            DialogUtils.showChoiceDialog(context, AppLocalizations.of(context).trans("logout"), AppLocalizations.of(context).trans("sure_logout"), (){
              _controller.logout(context);
            });
          })
        ],
      ),
      body: LoadingContainer(
        loadingStream: _controller.loadingStream,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(
                    AppLocalizations.of(context).trans("your_info").toUpperCase(),
                  style: AppTextStyle.headerStyle,
                ),
                SizedBox(height: contentSpacing,),
                TextFieldWhite(AppLocalizations.of(context).trans("first_name"), enabled: false, controller: _controller.firstNameController),
                SizedBox(height: contentSpacing,),
                TextFieldWhite(AppLocalizations.of(context).trans("email_address"), enabled: false, controller: _controller.emailController, ),
                SizedBox(height: contentSpacing,),
                TextFieldWhite(AppLocalizations.of(context).trans("phone_number"), enabled: false, controller: _controller.phoneController,),
                SizedBox(height: contentSpacing,),
                TextFieldWhite(AppLocalizations.of(context).trans("street"), enabled: false, controller: _controller.streetController),
                SizedBox(height: contentSpacing,),
                TextFieldWhite(AppLocalizations.of(context).trans("postal_code"), enabled: false, controller: _controller.postalCodeController,),
                SizedBox(height: contentSpacing,),
                TextFieldWhite(AppLocalizations.of(context).trans("city"), enabled: false, controller: _controller.cityController),
                SizedBox(height: contentSpacing,),
                TextFieldWhite(AppLocalizations.of(context).trans("country"), enabled: false, controller: _controller.countryController),
                SizedBox(height: contentSpacing,),
              ],
            ),
          ),
        ),
      ),
    );
  }

}

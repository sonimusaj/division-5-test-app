import 'package:flutter/material.dart';
import 'package:otto_wilde/utils/localization/app_localizations.dart';

class DialogUtils{
  static showMessageDialog(BuildContext context, String title, String message) {

    AlertDialog alert = AlertDialog(
      title: Text(title),
      content: Text(message),
      actions: [
        FlatButton(
          child: Text("OK"),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  static showChoiceDialog(BuildContext context, String title, String message, Function onOkPressed) {

    AlertDialog alert = AlertDialog(
      title: Text(title),
      content: Text(message),
      actions: [
        FlatButton(
          child: Text(AppLocalizations.of(context).trans("cancel")),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        FlatButton(
          child: Text("OK"),
          onPressed: () {
            Navigator.of(context).pop();
            onOkPressed();
          },
        ),
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
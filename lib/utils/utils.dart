import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Utils{
 static showToast(BuildContext context, String message){
   Fluttertoast.showToast(msg: message, gravity: ToastGravity.BOTTOM, textColor: Colors.white, backgroundColor: Colors.black45);
 }
}
import 'package:shared_preferences/shared_preferences.dart';


class SharedPreferencesUtil{
  static String _logged_in_user_id_key = 'logged_in_user_id_key';

  static saveLoggedInUserId(String id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(_logged_in_user_id_key, id);
  }

  static Future<String> getLoggedInUserId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(_logged_in_user_id_key) ?? "";
  }
}
